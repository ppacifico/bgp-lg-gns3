# BGP Looking Glass for GNS3
This is a basic implementation of a BGP Looking Glass service, intended to be used for testing within a GNS3 topology.
***

## How does it work?

- It uses `telnetlib` to create a connection handler between the route servers, and `flask` as a middleware to handle user requests.
- The `site.html` includes every router that can be accesed or used a LG server. Each router has a `value` field and its content indicates the port assigned by GNS3 that will be used to establish a terminal connection via `telnet`. Please change these values by the ones GNS3 assigned to each of your routers.
- To avoid leaving a terminal line opened, each message will include a login to the route server, run the desired command and then, logout.

### Prerequisites:
Get a GNS3 Topology up and running, using Cisco devices.

### Process
1. Start the flask server. By default, it will be started at `localhost or 127.0.0.1` port `5000`. Feel free to change that.
2. Head on to your favorite browser and type `127.0.0.1:5000`.
3. Select your favorite command, click `Go` and profit.

## Commands Supported
This implementation supports the three basic BGP commands:

- Ping (IPv4 & IPv6)
- Traceroute (IPv4 & IPv6)
- BGP Summary

## When should this be used?
This code is only intended for testing and educational purposes. It should not be deployed on a production environment.

Obviously, feel free to customize this code to fill up your needs.
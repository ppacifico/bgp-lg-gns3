from telnetlib import Telnet
import time

class LookingGlass:
    
    def __init__(self, host, port):
        self.__net_handler = Telnet(host, port)
    
    def __login(self):
        USER = b'cisco'
        PASS = b'cisco'
        self.__net_handler.write(b'\n\r')
        self.__net_handler.read_until(b'Username:')
        self.__net_handler.write(USER + b'\n')
        if PASS:
            self.__net_handler.read_until(b'Password:')
            self.__net_handler.write(PASS + b'\n')
    
    def __exit(self):
        self.__net_handler.write(b'exit\n')
    
    def ping(self, ip_address):
        self.__login()
        self.__net_handler.write(b'ping ' + ip_address.encode('ascii') + b'\n')
        time.sleep(3)
        self.__exit()
        return self.__net_handler.read_very_eager().decode('ascii')
    
    def traceroute(self, ip_address):
        self.__login()
        self.__net_handler.write(b'traceroute ' + ip_address.encode('ascii') + b'\n')
        time.sleep(7)
        self.__exit()
        return self.__net_handler.read_very_eager().decode('ascii')

    def bgp_summary(self):
        self.__login()
        self.__net_handler.write(b'show bgp summary\n')
        time.sleep(3)
        self.__exit()
        return self.__net_handler.read_very_eager().decode('ascii')
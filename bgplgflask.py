from flask import Flask, render_template, request
from bgplg import LookingGlass

bgplg = Flask(__name__)
GNS3_REMOTE_SERVER_ADDRESS = 'your_ip_address'

@bgplg.route('/')
def home():
   return render_template('site.html')

@bgplg.route('/', methods = ['POST'])
def get_values():
    router = request.form['routers']
    command = request.form['command']
    ip_address = request.form['ip']
    
    if command == 'ping':
        return ping_host(router, ip_address)
    elif command == 'trace':
        return trace_host(router, ip_address)
    elif command == 'bgpsum':
        return bgp_sum(router)

def ping_host(router, ip_address):
    output = LookingGlass(GNS3_REMOTE_SERVER_ADDRESS, int(router)).ping(ip_address)
    return '<h1> Ping Command </h1>' + '<xmp>' + output + '</xmp>'

def trace_host(router, ip_address):
    output = LookingGlass(GNS3_REMOTE_SERVER_ADDRESS, int(router)).traceroute(ip_address)
    return '<h1> Traceroute Command </h1>' + '<xmp>' + output + '</xmp>'

def bgp_sum(router):
    output = LookingGlass(GNS3_REMOTE_SERVER_ADDRESS, int(router)).bgp_summary()
    return '<h1> BGP Summary Command </h1>' + '<xmp>' + output + '</xmp>'


bgplg.run(debug = False)